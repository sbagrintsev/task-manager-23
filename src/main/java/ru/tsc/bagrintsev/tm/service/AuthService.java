package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PermissionDeniedException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;
import java.util.Arrays;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    public String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void signIn(
            @Nullable final String login,
            @Nullable final String password) throws AbstractException, GeneralSecurityException {
        check(EntityField.LOGIN, login);
        check(EntityField.PASSWORD, password);
        @NotNull final User user = userService.findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        if (!(HashUtil.generateHash(password, user.getPasswordSalt())).equals(user.getPasswordHash())) {
            throw new PasswordIsIncorrectException();
        }
        userId = user.getId();
    }

    @Override
    public void signOut() {
        userId = null;
    }

    @Nullable
    @Override
    public String getCurrentUserId() {
        return userId;
    }

    @NotNull
    @Override
    public User getCurrentUser() throws AbstractException {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findOneById(userId);
    }

    @Override
    public void checkRoles(@NotNull final Role[] roles) throws AbstractException {
        if (roles.length == 0) return;
        @NotNull final User user = getCurrentUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionDeniedException();
    }
}
