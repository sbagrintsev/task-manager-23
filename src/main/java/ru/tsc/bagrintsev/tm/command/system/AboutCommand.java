package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class AboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[Author]");
        System.out.println("Name: Sergey Bagrintsev");
        System.out.println("E-mail: sbagrintsev@t1-consulting.com");
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print about author.";
    }

}
