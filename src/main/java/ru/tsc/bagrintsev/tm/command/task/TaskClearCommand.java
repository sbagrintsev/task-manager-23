package ru.tsc.bagrintsev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class TaskClearCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CLEAR]");
        @Nullable final String userId = getUserId();
        getTaskService().clear(userId);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }
}
