package ru.tsc.bagrintsev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }
}
