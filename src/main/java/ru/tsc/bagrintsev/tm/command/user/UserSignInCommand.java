package ru.tsc.bagrintsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.security.GeneralSecurityException;

public final class UserSignInCommand extends AbstractUserCommand {

    @Override
    public void execute() throws IOException, AbstractException, GeneralSecurityException {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo(EntityField.PASSWORD);
        @NotNull final String password = TerminalUtil.nextLine();
        getAuthService().signIn(login, password);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-sign-in";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign user in.";
    }

}
