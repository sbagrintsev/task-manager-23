package ru.tsc.bagrintsev.tm.exception.user;

public final class LoginIsIncorrectException extends AbstractUserException {

    public LoginIsIncorrectException() {
        super("Error! User with this login does not exist...");
    }

}
