package ru.tsc.bagrintsev.tm.exception.entity;

public class IncorrectRoleException extends AbstractEntityException {

    public IncorrectRoleException() {
        super("Error! Role is incorrect...");
    }

}
