package ru.tsc.bagrintsev.tm.exception.user;

public class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Access denied! Authentication failed, check login or password...");
    }

}
