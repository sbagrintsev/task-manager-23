package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull final AbstractCommand command);

    @NotNull
    AbstractCommand getCommandByName(@NotNull final String name) throws CommandNotSupportedException;

    @NotNull
    AbstractCommand getCommandByShort(@NotNull final String shortName) throws ArgumentNotSupportedException;

    @NotNull
    Collection<AbstractCommand> getAvailableCommands();

}
